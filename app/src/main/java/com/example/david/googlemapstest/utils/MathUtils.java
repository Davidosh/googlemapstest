package com.example.david.googlemapstest.utils;

import com.example.david.googlemapstest.marker.ClusteredMarkerItem;

import java.util.Collection;
import java.util.Random;

public class MathUtils {

    public static double getRandomInRange(double rangeMin, double rangeMax) {
        Random r = new Random();
        if (rangeMin > rangeMax) {
            double t = rangeMax;
            rangeMax = rangeMin;
            rangeMin = t;
        }
        return rangeMin + (rangeMax - rangeMin) * r.nextDouble();
    }

    public static int getRandomInRange(int rangeMin, int rangeMax) {
        Random r = new Random();
        if (rangeMin > rangeMax) {
            int t = rangeMax;
            rangeMax = rangeMin;
            rangeMin = t;
        }
        return r.nextInt((rangeMax - rangeMin) + 1) + rangeMin;
    }

    public static int calculateAverageValueInMarkers(Collection<ClusteredMarkerItem> items) {
        if (items == null || items.size() == 0) {
            return 0;
        }
        int sum = 0;
        for (ClusteredMarkerItem item : items) {
            sum += item.getOwnValue();
        }
        return sum / items.size();
    }
}
