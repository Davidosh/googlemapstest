package com.example.david.googlemapstest;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.david.googlemapstest.marker.ClusteredMarkerItem;
import com.example.david.googlemapstest.marker.ClusteredItemView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import static com.example.david.googlemapstest.utils.MathUtils.getRandomInRange;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final double MIN_LATITUDE = 45;
    private static final double MAX_LATITUDE = 52;
    private static final double MIN_LONGITUDE = 22;
    private static final double MAX_LONGITUDE = 40;

    private ClusterManager<ClusteredMarkerItem> clusterManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        findViewById(R.id.fab).setOnClickListener(v -> refreshAllMarkers());
        clusterManager = new ClusterManager<>(getBaseContext(), googleMap);
        clusterManager.setRenderer(new ClusteredItemView(
                getBaseContext(), googleMap, clusterManager));
        googleMap.setOnCameraIdleListener(clusterManager);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng((MIN_LATITUDE + MAX_LATITUDE) / 2,
                (MIN_LONGITUDE + MAX_LONGITUDE) / 2)));
        addAllMarkers();
    }

    private void refreshAllMarkers() {
        clusterManager.clearItems();
        addAllMarkers();
        clusterManager.cluster();
    }

    private void addAllMarkers() {
        for (int i = 0; i < 100; i++) {
            clusterManager.addItem(new ClusteredMarkerItem(new MarkerOptions()
                    .position(new LatLng(getRandomInRange(MIN_LATITUDE, MAX_LATITUDE), getRandomInRange(MIN_LONGITUDE, MAX_LONGITUDE))))
                    .setOwnValue(getRandomInRange(1, 10)));
            clusterManager.cluster();
        }
    }
}
