package com.example.david.googlemapstest.marker;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

public class ClusteredMarkerItem implements ClusterItem {

    private MarkerOptions markerOptions;
    private int ownValue;

    public ClusteredMarkerItem(MarkerOptions markerOptions) {
        this.markerOptions = markerOptions;
    }

    public ClusteredMarkerItem setMarkerOptions(MarkerOptions markerOptions) {
        this.markerOptions = markerOptions;
        return this;
    }

    public ClusteredMarkerItem setOwnValue(int ownValue) {
        this.ownValue = ownValue;
        return this;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    @Override
    public LatLng getPosition() {
        return markerOptions.getPosition();
    }

    @Override
    public String getTitle() {
        return markerOptions.getTitle();
    }

    @Override
    public String getSnippet() {
        return markerOptions.getSnippet();
    }

    public int getOwnValue() {
        return ownValue;
    }


}
