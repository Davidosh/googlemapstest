package com.example.david.googlemapstest.marker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.david.googlemapstest.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import static com.example.david.googlemapstest.utils.MathUtils.calculateAverageValueInMarkers;
import static com.example.david.googlemapstest.utils.ViewUtils.createBitmapFromView;

public class ClusteredItemView extends DefaultClusterRenderer<ClusteredMarkerItem> {
    private Context context;

    public ClusteredItemView(Context context, GoogleMap map, ClusterManager<ClusteredMarkerItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusteredMarkerItem item,
                                               MarkerOptions markerOptions) {
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.custom_marker_layout, null);
        ((TextView) marker.findViewById(R.id.tv_custom_marker))
                .setText(String.valueOf(item.getOwnValue()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createBitmapFromView(marker)));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<ClusteredMarkerItem> cluster, MarkerOptions markerOptions) {
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_group_layout, null);
        ((TextView) marker.findViewById(R.id.tv_custom_marker))
                .setText(String.valueOf(calculateAverageValueInMarkers(cluster.getItems())));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createBitmapFromView(marker)));
    }


}
